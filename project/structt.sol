pragma solidity 0.5.1;

contract mycontract{

    person[] public people; // declaration of object
    uint256 public peoplecount;

    struct person{

        string firstname;
        string lastname;

    }

    function addperson(string memory firstname,string memory lastname)public{

        people.push(person(firstname,lastname));
        peoplecount ++;
        
    }

}