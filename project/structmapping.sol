pragma solidity 0.5.1;

contract mycontract{

    uint256 public peoplecount;
    mapping(uint => person)public people;

    struct person{

        uint id;
        string firstname;
        string lastname;

    }

    function addperson(string memory firstname,string memory lastname)public{
        
        peoplecount ++;
        people[peoplecount]=(person(peoplecount,firstname,lastname));
        
    }

}