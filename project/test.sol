pragma solidity 0.5.1;
contract testcon{
    
    //declar global variables and function
    string value;
    
    //constructor declaration
    constructor() public{
        value="hello"; 
    }
    
    //function declaration
    function get() public view returns(string memory){ 
        return value;

    }
    
    function set(string memory _value)public{
        value = _value;
        //function local variable
    }
}