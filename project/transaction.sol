pragma solidity 0.5.1;

contract mycontract {
    mapping(address => uint) public balance;
    address payable wallet;

    constructor(address payable _wallet)public{
        wallet=_wallet;
    }

    function buytoken()public payable{
        // buy a token eg not actuall token we are just incrementing a pointing value
        balance[msg.sender]+=1;
        // send ether to the wallet 
        //before deploying we need to assign the value in eather that is need to be transfered
        wallet.transfer(msg.value);
        //msg.value is got from value column from the ide
    }
}