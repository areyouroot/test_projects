pragma solidity 0.5.1;

contract mycon{
    enum state{wait,ready,active}
    state public status;
    constructor()public{
        status=state.wait;
    }
    function activate() public{
        status=state.active;
    }
    function isactive()public view returns(bool){
        return status == state.active;
    }
}