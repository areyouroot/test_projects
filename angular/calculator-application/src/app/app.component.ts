import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Calculator';
  input1: string = "";
  output: number = 0
  presskey(key: string) {
    if (key == "") {
      this.input1 = ''
    }
    else {
      this.input1 = this.input1 + key;
    }
    this.Calculator()
  }
  Calculator() {
    this.output = 0;
    let temp: number = 0;
    for (let index: number = 0; index < this.input1.length; index++) {
      if (this.input1[index] == '+') {
        index++
        temp = this.GetNumber(index)
        index += temp.toString().length - 1
        this.output += temp
      }
      else if (this.input1[index] == '-') {
        index++
        temp = this.GetNumber(index)
        index += temp.toString().length - 1
        this.output -= temp
      }
      else if (this.input1[index] == '*') {
        index++
        temp = this.GetNumber(index)
        index += temp.toString().length - 1
        this.output *= temp
      }
      else if (this.input1[index] == '/') {
        index++
        temp = this.GetNumber(index)
        index += temp.toString().length - 1
        this.output /= temp
      }
      else {
        this.output = this.GetNumber(index);
        index += (this.output).toString().length - 1
      }
    }
  }
  GetNumber(index: number): number {
    let temNum: string = '';
    while (index < this.input1.length) {
      if (this.input1[index] == '+') {
        break;
      }
      else if (this.input1[index] == '-') {
        break;
      }
      else if (this.input1[index] == '*') {
        break;
      }
      else if (this.input1[index] == '/') {
        break;
      }
      else {
        temNum += this.input1[index]
        console.log(temNum)
      }
      index++
    }
    return parseInt(temNum)
  }
}
