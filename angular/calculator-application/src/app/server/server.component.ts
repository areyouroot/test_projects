import { Component } from "@angular/core";

@Component({
    selector: 'app-server',
    templateUrl: './server.component.html'
})
export class ServerComponent {
    username: string = "string interpolation demo type anything in the above text box and click some ware else to see the magic";
    UpdateUser(event) {
        this.username = (<HTMLInputElement>event.target).value;
    }
    buttonStaus: boolean = true
    CheckUser() {
        if (this.username == "") {
            this.buttonStaus = true
        }
        else {
            this.buttonStaus = false
        }
    }
}